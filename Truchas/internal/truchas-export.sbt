<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="export" BaseType="operation" Label="Export to Truchas" Version="1">
      <BriefDescription>
        Write Truchas input file.
      </BriefDescription>
      <DetailedDescription>
        Using the specified model and simulation attribute resources, this
        operation will write a Truchas input file to the specified location.
      </DetailedDescription>
      <ItemDefinitions>
        <Component Name="model" Label="Model" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::model::Resource" Filter="model" />
          </Accepts>
        </Component>
        <Resource Name="attributes" Label="Attributes" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::attribute::Resource"/>
          </Accepts>
        </Resource>
        <File Name="output-file" Label="Output File"
          FileFilters="Input files (*.inp);;All files (*.*)" Version="0">
          <BriefDescription>Truchas file to be generated</BriefDescription>
        </File>
        <File Name="mesh-file" Label="Mesh File" ShouildExist="true"
          FileFilters="Exodus files (*.exo *.ex2 *.gen);;All files (*.*)" Version="0">
          <BriefDescription>Mesh file used by all physics solvers other than induction heating.</BriefDescription>
        </File>
        <File Name="alt-mesh-file" Label="Alt Mesh File" Optional="true" IsEnabledByDefault="false"
          ShouildExist="true"
          FileFilters="Exodus files (*.exo *.ex2 *.gen);;All files (*.*)" Version="0">
          <BriefDescription>Alternate mesh file used by the induction heating solver.</BriefDescription>
        </File>
      </ItemDefinitions>
     </AttDef>
   </Definitions>
  <Views>
    <View Type="Instanced" Title="Export Settings" TopLevel="true"
      FilterByCategory="false" FilterByAdvanceLevel="true">
      <InstancedAttributes>
        <Att Name="Options" Type="export" />
      </InstancedAttributes>
    </View>
  </Views>
  </SMTK_AttributeResource>
