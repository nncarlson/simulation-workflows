<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
    <Cat>Induction Heating</Cat>
    <Cat>Solid Mechanics</Cat>
  </Categories>
  <!-- Attribute Definitions-->
  <Definitions>
    <AttDef Type="background-material" Label="Default Material" BaseType="" Version="0" Unique="true">
      <BriefDescription>Material to use to fill any portion of the mesh not explicitlydefined in the "Body" namelists.</BriefDescription>
      <ItemDefinitions>
        <Component Name="background-material" Label="Select" NumberOfRequiredValues="1" Unique="true" Version="1">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='phase']"/>
          </Accepts>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
        </Component>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="phase" BaseType="" Abstract="true" Unique="true">
      <AssociationsDef Name="MaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="phase.void" BaseType="phase" Label="Void" Version="0">
      <ItemDefinitions>
        <Double Name="void-temperature" Label="Temperature">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="sound-speed" Label="Speed of Sound" AdvanceLevel="1">
          <BriefDescription>| The adiabatic sound speed that is used in computing thecompressibility of each cell containing the material.
This is not a real sound speed, but a numerical artifice
used to permit collapse of small void bubbles.</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="phase.material" BaseType="phase" Label="Phase" Version="0">
      <ItemDefinitions>
        <Double Name="density" Label="Density (rho)" Optional="false">
          <BriefDescription>Mass density of the material phase</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="density-deviation" Label="Density Deviation" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>The relative deviation of the true temperature-dependent
density from the reference density</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <ExpressionType>fn.material.density-deviation</ExpressionType>
        </Double>
        <Double Name="conductivity" Label="Conductivity (K)">
          <BriefDescription>Thermal conductivity of the material phase</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <ExpressionType>fn.material.conductivity</ExpressionType>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="specific-heat" Label="Specific Heat (Cp)">
          <BriefDescription>Specific heat of the material phase</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <ExpressionType>fn.material.specific-heat</ExpressionType>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
        <Group Name="fluid" Label="Fluid" Optional="true" IsEnabledByDefault="false">
          <ItemDefinitions>
            <Double Name="viscosity" Label="Viscosity (nu)">
              <BriefDescription>The dynamic viscosity of a fluid phase</BriefDescription>
              <Categories>
                <Cat>Fluid Flow</Cat>
                <Cat>Heat Transfer</Cat>
                <Cat>Induction Heating</Cat>
                <Cat>Solid Mechanics</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
              <ExpressionType>fn.material.viscosity</ExpressionType>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
          </ItemDefinitions>
        </Group>
        <!-- Electromagnetic properties-->
        <Group Name="electrical-properties" Label="Electrical Properties">
          <ItemDefinitions>
            <Double Name="electrical-conductivity" Label="Electrical Conductivity">
              <DefaultValue>0.0</DefaultValue>
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
            </Double>
            <Double Name="electrical-susceptibility" Label="Electrical Susceptibility" AdvanceLevel="1">
              <DefaultValue>0.0</DefaultValue>
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
            </Double>
            <Double Name="magnetic-susceptibility" Label="Magnetic Susceptibility" AdvanceLevel="1">
              <DefaultValue>0.0</DefaultValue>
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="phase-transition" BaseType="" Label="Phase Transition" Version="0">
      <ItemDefinitions>
        <Component Name="lower" Label="Low-Temperature phase" NumberOfRequiredValues="1">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='phase.material']"/>
          </Accepts>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
        </Component>
        <Double Name="latent-heat" Label="Latent Heat (Lf)">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="lower-transition-temperature" Label="Low Transition Temperature (Ts)">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
        </Double>
        <Double Name="upper-transition-temperature" Label="High Transition Temperature (Tl)">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>100.0</DefaultValue>
        </Double>
        <Double Name="smoothing-radius" Label="Smoothing Radius" NumberOfRequiredValues="1" Version="0">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <BriefDescription>The transition smoothing function. Default 0.25 times delta T.</BriefDescription>
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Component Name="upper" Label="High-Temperature Phase" NumberOfRequiredValues="1">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='phase.material']"/>
          </Accepts>
        </Component>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
