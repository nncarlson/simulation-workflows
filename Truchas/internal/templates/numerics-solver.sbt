<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
    <Cat>Solid Mechanics</Cat>
  </Categories>
  <Definitions>
    <!-- Numerics-->
    <AttDef Type="numerics" Label="Numerics" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="dt_init" Label="Dt_Init">
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <BriefDescription>Integration time step value used for
the first computation cycle</BriefDescription>
          <DefaultValue>1.0e-6</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="dt_grow" Label="Dt_Grow">
          <BriefDescription>A factor to multiply the current integration time step
when estimating the next cycle time step.</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>1.05</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">1.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="dt_max" Label="Dt_Max">
          <BriefDescription>Maximum allowable value for the time step.</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>10.0</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
        <Double Name="dt_min" Label="Dt_Min">
          <BriefDescription>Minimum allowable value for the time step.</BriefDescription>
          <Categories>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue>1.0e-6</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0.0</Min>
          </RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>
    <!-- Simulation Control-->
    <AttDef Type="simulation-control" Label="Simulation Control" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <Group Name="simulation-control" Label="Simulation Control" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>This item is superseded by the displacement sequence when
moving-enclosure radiation is enabled</BriefDescription>
          <ItemDefinitions>
            <Double Name="phase-start-times" Label="Phase Start Times" Extensible="true">
              <BriefDescription>The list of starting times of each of the phases</BriefDescription>
              <Categories>
                <Cat>Fluid Flow</Cat>
                <Cat>Heat Transfer</Cat>
                <Cat>Solid Mechanics</Cat>
              </Categories>
            </Double>
            <Double Name="phase-init-dt-factor" Label="Phase Init Dt Factor">
              <Categories>
                <Cat>Fluid Flow</Cat>
                <Cat>Heat Transfer</Cat>
                <Cat>Solid Mechanics</Cat>
              </Categories>
            </Double>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
    <!-- Thermal Solver Settings-->
    <AttDef Type="ht.solver" Label="Solver" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <String Name="analysis" Label="Analysis Type">
          <Categories>
            <Cat>Heat Transfer</Cat>
          </Categories>
          <ChildrenDefinitions>
            <!-- Thermal-only case-->
            <Group Name="thermal" Label="Thermal Only">
              <ItemDefinitions>
                <Double Name="abs-enthalpy-tol" Label="Absolute enthalpy tolerance">
                  <BriefDescription>The tolerance for the absolute error component of the
enthalpy error norm used by the BDF2 integrator</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
                <Double Name="rel-enthalpy-tol" Label="Relative enthalpy tolerance">
                  <BriefDescription>The tolerance for the relative error component of the
enthalpy error norm used by the BDF2 integrator</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.001</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                    <Max Inclusive="true">1.0</Max>
                  </RangeInfo>
                </Double>
                <Double Name="abs-temperature-tol" Label="Absolute temperature tolerance">
                  <BriefDescription>The tolerance for the absolute error component of the
temperature error norm used by the BDF2 integrator</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.0</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                  </RangeInfo>
                </Double>
                <Double Name="rel-temperature-tol" Label="Relative temperature tolerance">
                  <BriefDescription>The tolerance for the relative error component of the
temperature error norm used by the BDF2 integrator</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.001</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                    <Max Inclusive="true">1.0</Max>
                  </RangeInfo>
                </Double>
                <Void Name="verbose-stepping" Label="Verbose stepping" Optional="true" IsEnabledByDefault="true">
                  <BriefDescription>Enables the output of detailed BDF2 time stepping information</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                </Void>
                <Int Name="max-nlk-itr" Label="Max NLK iterations" AdvanceLevel="1">
                  <BriefDescription>The maximum number of NLK nonlinear solver iterations allowed</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>5</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">2</Min>
                  </RangeInfo>
                </Int>
                <Int Name="max-nlk-vec" Label="Max NLK vectors" AdvanceLevel="1">
                  <BriefDescription>The maximum number of acceleration vectors to use
in the NLK nonlinear solver</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>4</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
                <Double Name="nlk-tol" Label="NLK convergence tolerance" AdvanceLevel="1">
                  <BriefDescription>The convergence tolerance for the NLK nonlinear solver</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.05</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">0.0</Min>
                    <Max Inclusive="true">1.0</Max>
                  </RangeInfo>
                </Double>
                <Double Name="nlk-vec-tol" Label="NLK vector drop tolerance" AdvanceLevel="1">
                  <BriefDescription>The NLK vector drop tolerance</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>0.001</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="false">0.0</Min>
                  </RangeInfo>
                </Double>
                <Int Name="pc-amg-cycles" Label="Preconditioning V-cycles" AdvanceLevel="1">
                  <BriefDescription>The number of V-cycles to take per preconditioning step
of the nonlinear iteration</BriefDescription>
                  <Categories>
                    <Cat>Heat Transfer</Cat>
                  </Categories>
                  <DefaultValue>2</DefaultValue>
                  <RangeInfo>
                    <Min Inclusive="true">1</Min>
                  </RangeInfo>
                </Int>
              </ItemDefinitions>
            </Group>
            <!-- Thermal plus flow case-->
            <Group Name="thermal-plus-fluid" Label="Thermal plus Fluid">
              <ItemDefinitions>
                <Group Name="thermal-solver" Label="Thermal Solver">
                  <ItemDefinitions>
                    <Double Name="residual-atol" Label="Absolute residual tolerance">
                      <BriefDescription>The absolute residual tolerance used by the iterative
nonlinear solver of the non-adaptive integrator</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>0.0</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                      </RangeInfo>
                    </Double>
                    <Double Name="residual-rtol" Label="Relative residual tolerance">
                      <BriefDescription>The relative residual tolerance used by the iterative
nonlinear solver of the non-adaptive integrator</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>1.0e-8</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                        <Max Inclusive="true">1.0</Max>
                      </RangeInfo>
                    </Double>
                    <Int Name="max-nlk-itr" Label="Max NLK iterations">
                      <BriefDescription>The maximum number of NLK nonlinear solver iterations allowed</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>5</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">2</Min>
                      </RangeInfo>
                    </Int>
                    <Int Name="max-nlk-vec" Label="Max NLK vectors">
                      <BriefDescription>The maximum number of acceleration vectors to use
in the NLK nonlinear solver</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>4</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">1</Min>
                      </RangeInfo>
                    </Int>
                    <Void Name="verbose-stepping" Label="Verbose stepping" Optional="true" IsEnabledByDefault="false">
                      <BriefDescription>Enables the output of detailed BDF2 time stepping information</BriefDescription>
                    </Void>
                    <Double Name="nlk-vec-tol" Label="NLK vector drop tolerance" AdvanceLevel="1">
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>0.001</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="false">0.0</Min>
                      </RangeInfo>
                    </Double>
                    <Int Name="pc-amg-cycles" Label="Preconditioning V-cycles" AdvanceLevel="1">
                      <BriefDescription>The number of V-cycles to take per preconditioning
step of the nonlinear iteration</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>1</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">1</Min>
                      </RangeInfo>
                    </Int>
                    <Double Name="cond-vfrac-threshold" Label="Conduction volume fraction" AdvanceLevel="1">
                      <BriefDescription>Cond_Vfrac_Threshold: Material volume fraction threshold
for inclusion in heat conduction when using the non-adaptive integrator.</BriefDescription>
                      <Categories>
                        <Cat>Heat Transfer</Cat>
                      </Categories>
                      <DefaultValue>0.001</DefaultValue>
                      <RangeInfo>
                        <Min Inclusive="true">0.0</Min>
                        <Max Inclusive="true">1.0</Max>
                      </RangeInfo>
                    </Double>
                  </ItemDefinitions>
                </Group>
              </ItemDefinitions>
            </Group>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Thermal Only">thermal</Value>
              <Items>
                <Item>thermal</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Fluid plus Thermal">thermal-plus-fluid</Value>
              <Items>
                <Item>thermal-plus-fluid</Item>
                <Item>fluid</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
