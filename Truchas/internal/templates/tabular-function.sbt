<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">
  <!--
    Because the top-level view is a category view, you
    MUST SET CATEGORIES ON EXPRESSIONS. And to do so,
    add a void item def *after* the ValuePairs group def.
  -->
  <Categories>
    <Cat>Enclosure Radiation</Cat>
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
    <Cat>Induction Heating</Cat>
    <Cat>Solid Mechanics</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="sim-expression" Abstract="1" Association="None"/>
    <AttDef Type="tabular-function" Label="Tabular Function" BaseType="sim-expression" Version="0">
      <ItemDefinitions>
        <Group Name="ValuePairs" Label="Value Pairs" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <Double Name="X" Version="0" AdvanceLevel="0" NumberOfRequiredValues="0" Extensible="true"/>
            <Double Name="Value" Version="0" AdvanceLevel="0" NumberOfRequiredValues="0" Extensible="true"/>
          </ItemDefinitions>
        </Group>
        <Void Name="CategoryPlaceholder" AdvanceLevel="99">
          <Categories>
            <Cat>Enclosure Radiation</Cat>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
