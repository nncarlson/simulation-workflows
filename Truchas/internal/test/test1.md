Steps for (re)constructing test case #1
Model: disk_out_ref.smtk

Modules/Enclosure Radiation
* Disable!

Globals: Functions
* New
* Rename "Al viscosity"
* (1)  662, 1379
* (2)  669, 1364
* (3)  833, 1028
* Remove rows 4-10 (NOT Delete!)

* New
* Rename "Source Function"
* 100, 100.001
* 200, 200.002
* 300, 300.003
* Remove rows 4-10 (NOT Delete!)

Materials/Specification
* New
* Name "Aluminum 6061-T6"
* Two Phase
* Density 2700
* Conductivity 167
* Viscosity function "Al viscosity"
* Thermal specificaton "Specific Heat" 
* Specific heat 896
* Latent heat 321
* Solidus Temperature 582
* Liquidus Temperature 652
* Associated to block id 1

Material
* Name "Void Material"
* Void
* Temperature 20

Background material
* Set to "Void material"


## Modules/Heat Transfer/Boundary Conditions/Boundary
Dirichlet --> New
* Rename: "bc temp"
* Temperature 98.6
* Associate to Side Set 1

Flux --> New
* Rename "bc flux"
* Heat Flux 3.14
* Associate to Side Set 2


HTC --> New
* Rename "bc htc"
* Heat Transfer Coefficient (h) 0.543
* Reference Temperature (T0) 32.2
* Associate to Side Set 3


Radiation --> New
* Rename "bc rad"
* Emissitivity (epsilon) 0.888
* Ambient Temp (T infinity) 32.2
* Associate to Side Set 4


## Modules/Heat Transfer/Boundary Conditions/Interface
Gap Radiation --> New
* Rename "ic rad"
* Emissivity (epsilon) 0.5
- Associate to Side Set 5

HTC --> New
* Rename "ic htc"
* Heat Transfer coefficient 0.123
* Associate to Side Set 6

Surface: Fluid Boundary Conditions
* Name "Pressure BC"
* Variable "Pressure"
* Boundary condition type "Dirichlet"
* Value 73.73
* Inflow checked
* Inflow material "Aluminum 6061-T6"
* Inflow temperature 707.7
* Associated to set id 2

Surface: Fluid Boundary Conditions
* Name "Velocity BC"
* Variable Velocity
* Boundary Condition Type Dirichlet
* Velocity Subgroup 0 1.1 2.2 3.3
* Associated to set id 3

## Modules/Heat Transfer/Solver
* Analysis Type "Fluid Plus Thermal"
* Thermal plus fluid: Thermal solver: Vebose Stepping ON
* Fluid Only: Flow numerics: Viscous Number 5
* Fluid Only: Flow Numerics: Mass limiter ON; cutoff 2.5e-5 (advanced)
* Viscous Flow Model enable

Analysis
SHOW ADVANCED LEVEL
General:
* Start Time 0
* End Time 999.9
* Output Delta-Time Multiplier 0.02

* (Advanced) Add Sub Group (2X)
* 1, 0.1
* 5, 0.2

Simulation Control
* Enable
* Phase Start Times Add 2 values 0, 20
* Phase Init Dt Factor 0.01

Initial Conditions
* Temperature --> New
* Name "body-0" (default)
* Temperature 98.6
* Associated to Element Block 1

Modules/Heat Transfer/Sources
Volumetric Heat Source --> New
* Name "Heat source"
* Temperature function "Source function"
* Associated to Element Block 1

Globals/
* Fluid Body Force: 0, 0, -32.2

Other
(Advanced) Exodus Block Modulus enabled
* 10000 (default)

Probes
Probe --> New
* Name "Probe 1"
* Description: Descriptive phrase goes here...
* Coordinates 3.14159, -0.111, 2.54

Save Resource As: sbi.test1.smtk
