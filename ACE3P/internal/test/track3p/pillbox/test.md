# Test conditions for pillbox omega3p/track3p

## Model
Pillbox.smtk (.gen)

## Load ACE3P.crf


# Step 1: Omega3P Analysis
Show by Category: Track3P

## Boundary conditions
Side set 1 --> Exterior
Side set 2 --> Exterior
Side set 6 --> Exterior

## Analysis
Number of eigenmodes searched - 2


# Step 2: Track3P Analysis
Show by Category: Track3P

## Analysis
Write Particles Impact Information --> on (checked)

Field Directory ---------------------> {NERSC folder}
Mode Number -------------------------> 0


## Field Scaling
Type ---------> Field Gradient (V/m)

Start Point --> 0, 0, -0.057
End Point ----> 0, 0, 0.057

Scan Token ---> 1: Scan Field Level

Minimum ------> 23.e6
Maximum ------> 25.e6
Interval -----> 1.e6


## Emitter
Emitter ----------------> New
Type -------------------> Electronic Surface
Region Bounds Min (m) --> 0.03, 0.04, -0.045
Region Bounds Max (m) --> 0.035, 0.1, 0.0


## Material
Simulation Model --> Multipacting

Side set 1 --------> Absorber
Side set 2 --------> Absorber
Side set 6 --------> Primary
  Emitter ---------> Emitter-0
  Secondary -------> on (checked)


## Postprocess
Enhancement Counter ----------> on (checked)

  SEY Surface 1 -----> on (checked)
  Boundary Surface --> side set 6
  SEY Filename ------> copper.dat
    (in ACE3P/internal/test/track3p/pillbox/)
  Minimum Enhancement Counter --> 1

## Save Simulation
pillbox.crf
