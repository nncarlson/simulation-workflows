# Test Conditions in t3p-test2.crf
This test case adds
* Mode voltage monitor
* Power monitor
* Automatic moving window

## Model
pillboxwg.smtk (.gen)


## Category
Show by Category: T3P


## Boundary conditions
Side set 1 --> Magnetic
Side set 2 --> Magnetic
Side set 3 --> Absorbing
Side set 4 --> Absorbing
Side set 5 --> Exterior
Side set 6 --> Exterior


## Analysis
Linear Solver --> MUMPS
Maximum Time ---> 5e-9
Time Step ------> 1e-12
Global Order ---> 0
Automatic Moving Window --> checked
* Region Order ----> 2
* Back Distance ---> 0.001
* Front Distance --> 0.002
* Structure End ---> 0.0689


## Loading
Dipole Loading --> New
* Coordinate ----> 1, 2, 3
* Tolerance -----> 0.002
* Power ---------> 0.75
* Phase ---------> 90

Port Mode Loading ------> New
* Boundary Port --------> side set 4
* Origin ---------------> 0.5, 1.0, 0.1
* E Field X Data File --> efx.prn


## Monitors
Mode Voltage Monitor --> New
* Name ----------------> modecoeff
* Monitor Port --------> side set 5

Power Monitor ---------> New
* Name ----------------> power
* Monitor Boundary ----> side set 1
* End Time ------------> NOT checked
* Time Intervals ------> 2e-12


## Save simulation
internal/test/t3p/cavity-quarter/t3p-test2.crf
