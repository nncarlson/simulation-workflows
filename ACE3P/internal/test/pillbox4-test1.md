# Test Conditions in pillbox4-test1.crf

## Model File
simulation-workflows/ACE3P/ACE3P-V2/internal/test/pillbox4.smtk

## Attributes
Set category to Omega3P


## Boundary conditions
side set 1 ------> Magnetic
side set 2 ------> Magnetic
side set 6 ------> Exterior
  Conductivity --> 5.78e+07


## Materials
Material --> New, element block 1
  Relative Permittivity (Epsilon) --> 0.99
  Relative Permeability (Mu) --> 0.98


## Analysis
Number of eigenmodes searched --> 4
