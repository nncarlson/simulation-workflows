<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>Omega3P</Cat>
    <Cat>S3P</Cat>
    <Cat>T3P</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="SurfaceProperty"
            Label="Surface Boundary Condition"
            BaseType="" Version="0" Unique="true" Abstract="true">
      <AssociationsDef Name="SurfacePropertyAssociations"
                       Version="0"
                       NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>face</MembershipMask>
      </AssociationsDef>
    </AttDef>
    <AttDef Type="Electric"
            Label="Electric"
            BaseType="SurfaceProperty" Version="0" Unique="true">
      <ItemDefinitions>
        <Void Name="Dummy" AdvanceLevel="11">
          <Categories>
            <Cat>S3P</Cat>
            <Cat>T3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Magnetic"
            Label="Magnetic"
            BaseType="SurfaceProperty" Version="0" Unique="true">
      <ItemDefinitions>
        <Void Name="Dummy" AdvanceLevel="11">
          <Categories>
            <Cat>S3P</Cat>
            <Cat>T3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Absorbing"
            Label="Absorbing"
            BaseType="SurfaceProperty" Version="0" Unique="true">
      <ItemDefinitions>
        <Void Name="Dummy" AdvanceLevel="11">
          <Categories>
            <Cat>S3P</Cat>
            <Cat>T3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="ConductiveSurface"
            BaseType="SurfaceProperty" Version="0" Unique="true" Abstract="true">
    </AttDef>
    <AttDef Type="Exterior"
            Label="Exterior"
            BaseType="ConductiveSurface" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="Sigma" Label="Conductivity" Version="0"
                    AdvanceLevel="0" NumberOfRequiredValues="1" Units="s/m">
          <BriefDescription>(Sigma) Surface Conductivity.</BriefDescription>
          <DefaultValue>5.8e7</DefaultValue>
          <Categories>
            <Cat>S3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
        </Double>
        <!-- T3P has no conductivity, so need dummy item here -->
        <Void Name="CategoryPlaceholder" AdvanceLevel="11">
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Impedance"
            Label="Impedance"
            BaseType="ConductiveSurface" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="Sigma" Label="Conductivity" Version="0"
                    AdvanceLevel="0" NumberOfRequiredValues="1" Units="s/m">
          <BriefDescription>(Sigma) Impedance Surface Conductivity.</BriefDescription>
          <DefaultValue>5.8e7</DefaultValue>
          <Categories>
            <Cat>Omega3P</Cat>
            <Cat>S3P</Cat>
            <Cat>T3P</Cat>
          </Categories>
        </Double>
        <Double Name="Frequency" Label="Frequency" Version="0"
                    AdvanceLevel="0" NumberOfRequiredValues="1" Units="Hz">
          <BriefDescription>Frequency for Conductivity specified.</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </Double>      
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="Periodic"
            Label="Periodic"
            BaseType="SurfaceProperty" Version="0" Unique="true">
      <ItemDefinitions>
        <Double Name="Theta" Label="Theta (Relative Phase Angle)" Version="0"
                    AdvanceLevel="0" NumberOfRequiredValues="1">
          <BriefDescription>Relative Phase Between Master and Slave Surfaces</BriefDescription>
          <DefaultValue>0.0</DefaultValue>
          <Categories>
            <Cat>Omega3P</Cat>
          </Categories>
        </Double>
        <ModelEntity Name="MasterSurface" Label="Master Surface" Version="0"
                    AdvanceLevel="0" NumberOfRequiredValues="1">
          <MembershipMask>face</MembershipMask>
          <Categories>
            <Cat>Omega3P</Cat>
          </Categories>
        </ModelEntity>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="Waveguide"
            Label="Port/Waveguide"
            BaseType="SurfaceProperty" Version="0" Unique="true">
      <ItemDefinitions>
        <Int Name="NumModes" Label="Number of Modes" Version="0"
                    AdvanceLevel="0" NumberOfRequiredValues="1">
          <BriefDescription>Number of Modes Loaded on Port</BriefDescription>
          <Categories>
            <Cat>S3P</Cat>
            <Cat>T3P</Cat>
            <Cat>Omega3P</Cat>
          </Categories>
          <DefaultValue>1</DefaultValue>
          <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
   <AttDef Type="HFormulation" Label="HFormulation" AdvanceLevel="99" Version="0">
    <ItemDefinitions>
      <Void Name="HFormulation" Label="HFormulation" Version="0"
            Optional="true" IsEnabledByDefault="false">
        <Categories>
          <Cat>S3P</Cat>
          <Cat>Omega3P</Cat>
        </Categories>
      </Void>
    </ItemDefinitions>
   </AttDef>
  </Definitions>
</SMTK_AttributeSystem>
