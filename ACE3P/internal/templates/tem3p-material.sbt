<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>TEM3P-Eigen</Cat>
    <Cat>TEM3P-Harmonic</Cat>
    <Cat>TEM3P-Elastic</Cat>
    <Cat>TEM3P-Linear-Thermal</Cat>
    <Cat>TEM3P-Nonlinear-Thermal</Cat>
    <Cat>TEM3P-ThermoElastic</Cat>
  </Categories>
  <Definitions>
    <AttDef Type="TEM3PElasticMaterial" Label="Material" BaseType="" Unique="true" Version="0">
      <AssociationsDef Name="TEM3PMaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="Material" Label="Material" Version="0">
          <Categories>
            <Cat>TEM3P-Eigen</Cat> 
            <Cat>TEM3P-Harmonic</Cat> 
            <Cat>TEM3P-Elastic</Cat> 
            <Cat>TEM3P-ThermoElastic</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Double Name="YoungsModulus" Label="Young's Modulus" Version="0" Units="Pa">
              <Categories>
                <Cat>TEM3P-Eigen</Cat> 
                <Cat>TEM3P-Harmonic</Cat> 
                <Cat>TEM3P-Elastic</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
              <RangleInfo>
                <Min Exclusive="true">0.0</Min>
              </RangleInfo>
            </Double>
            <Double Name="PoissonsRatio" Label="Poisson's Ratio" Version="0">
              <Categories>
                <Cat>TEM3P-Eigen</Cat> 
                <Cat>TEM3P-Harmonic</Cat> 
                <Cat>TEM3P-Elastic</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
              <RangleInfo>
                <Min Exclusive="true">0.0</Min>
              </RangleInfo>
            </Double>
            <Double Name="ElasticAlpha" Label="Elastic Alpha" Version="0" Optional="true" IsEnabledByDefault="true" Units="1/K">
              <BriefDescription>Thermal expansion coefficient (not relevant for eigenmode solver)</BriefDescription>
              <Categories>
                <Cat>TEM3P-Harmonic</Cat> 
                <Cat>TEM3P-Elastic</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
            <Double Name="Density" Label="Density" Units="kg/m^3" Version="0">
              <BriefDescription>Material density (only relevant for eigenmode solver)</BriefDescription>
              <Categories>
                <Cat>TEM3P-Eigen</Cat>
              </Categories>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <!-- The values are keywords to a lookup table in the writer-->
            <Value Enum="Nb, 2K">Nb-2K</Value>
            <Value Enum="Nb, 293K"> Nb-293K</Value>
            <Value Enum="Nb-Ti, 2K">NbTi-2K</Value>
            <Value Enum="Nb-Ti, 293K">NbTi-293K</Value>
            <Value Enum="Ti, 2K">Ti-2K</Value>
            <Value Enum="Ti, 293K">Ti-293K</Value>
            <Value Enum="Stainless Steel, 2K">StainlessSteel-2K</Value>
            <Value Enum="Stainless Steel, 293K">StainlessSteel-293K</Value>
            <Value Enum="Copper, 293K">Copper-293K</Value>
            <Structure>
              <Value Enum="Custom...">Custom</Value>
              <Items>
                <Item>YoungsModulus</Item>
                <Item>PoissonsRatio</Item>
                <Item>ElasticAlpha</Item>
                <Item>Density</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="TEM3PThermalMaterial" Label="Material" BaseType="" Unique="true" Version="0">
      <AssociationsDef Name="TEM3PMaterialAssociations" Version="0" NumberOfRequiredValues="0" Extensible="true">
        <MembershipMask>volume</MembershipMask>
      </AssociationsDef>
      <ItemDefinitions>
        <String Name="NonlinearMaterial" Label="Material" Version="0">
          <Categories>
            <Cat>TEM3P-Nonlinear-Thermal</Cat> 
            <Cat>TEM3P-ThermoElastic</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Double Name="ConstantThermalConductivity" Label="Thermal Conductivity" Units="W/m*K" Version="0">
              <BriefDescription>Thermal conductivity</BriefDescription>
              <Categories>
                <Cat>TEM3P-Nonlinear-Thermal</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
            </Double>
            <File Name="NonlinearThermalConductivity" Label="Input File" NumberOfRequiredValues="1" ShouldExist="true" Version="0">
              <BriefDescription>File that defines the function for the material nonlinearity</BriefDescription>
              <Categories>
                <Cat>TEM3P-Nonlinear-Thermal</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
            </File>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <!-- The values are filenames to be included in the input deck-->
            <Value Enum="Aluminum 6061">AL6061</Value>
            <Value Enum="Aluminum 6063">AL6063</Value>
            <Value Enum="AluminumOxide">Al203</Value>
            <Value Enum="Copper with RRR10">CURRR10</Value>
            <Value Enum="Copper with RRR30">CURRR30</Value>
            <Value Enum="Copper with RRR50">CURRR50</Value>
            <Value Enum="Copper with RRR100">CURRR100</Value>
            <Value Enum="Nb with RRR300">NbRRR300</Value>
            <Value Enum="NbTi">NbTi</Value>
            <Value Enum="Plastic Peek">Peek</Value>
            <Value Enum="Regular Nb">RGNB</Value>
            <Value Enum="Silicon Bronze">SiliconBronze</Value>
            <Value Enum="Stainless Steel 316">SS316</Value>
            <Structure>
              <Value Enum="Custom - Constant...">CustomConstant</Value>
              <Items>
                <Item>ConstantThermalConductivity</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Custom - Nonlinear...">CustomNonlinear</Value>
              <Items>
                <Item>NonlinearThermalConductivity</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
        <Double Name="ConstantThermalConductivity" Label="Thermal Conductivity" Units="W/m*K" Version="0">
          <BriefDescription>Thermal conductivity</BriefDescription>
          <Categories>
            <Cat>TEM3P-Linear-Thermal</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>
