<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>TEM3P-Eigen</Cat>
    <Cat>TEM3P-Elastic</Cat>
    <Cat>TEM3P-ThermoElastic</Cat>
  </Categories>
  <Definitions>
    <AttDef Type="MeshDump" Label="Mesh Dump" BaseType="" Version="0">
      <ItemDefinitions>
        <Group Name="DeformedMeshFiles" Label="Write Deformed Mesh Files" Version="0">
          <ItemDefinitions>
            <Void Name="WriteDeformedMesh" Label="Write Deformed Mesh" Optional="true" IsEnabledByDefault="true" Version="0">
              <BriefDescription>Write the deformed mesh</BriefDescription>
              <Categories>
                <Cat>TEM3P-Eigen</Cat> 
                <Cat>TEM3P-Elastic</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
            </Void>
            <Group Name="WriteDeformedEMMesh" Label="Write Deformed EM Mesh" Optional="true" IsEnabledByDefault="false" Version="0">
              <ItemDefinitions>
                <String Name="Source" Label="EM Mesh Folder" Version="0">
                  <BriefDescription> The source for the omega3p input mesh</BriefDescription>
                  <Categories>
                    <Cat>TEM3P-Eigen</Cat> 
                    <Cat>TEM3P-Elastic</Cat> 
                    <Cat>TEM3P-ThermoElastic</Cat>
                  </Categories>
                  <ChildrenDefinitions>
                    <String Name="NERSCDirectory" Label="NERSC Directory" Version="0">
                      <BriefDescription>Full path to existing directory on a NERSC file system. where the omega3p results are stored</BriefDescription>
                      <Categories>
                        <Cat>TEM3P-Eigen</Cat> 
                        <Cat>TEM3P-Elastic</Cat> 
                        <Cat>TEM3P-ThermoElastic</Cat>
                      </Categories>
                    </String>
                  </ChildrenDefinitions>
                  <DiscreteInfo DefaultIndex="0">
                    <Structure>
                      <Value Enum="NERSC Directory">NERSCDirectory</Value>
                      <Items>
                        <Item>NERSCDirectory</Item>
                      </Items>
                    </Structure>
                  </DiscreteInfo>
                </String>
              </ItemDefinitions>
            </Group>
            <Double Name="MeshDeformScale" Label="Mesh Deform Scale" Version="0">
              <BriefDescription>Deformation scale factor</BriefDescription>
              <Categories>
                <Cat>TEM3P-Eigen</Cat> 
                <Cat>TEM3P-Elastic</Cat> 
                <Cat>TEM3P-ThermoElastic</Cat>
              </Categories>
              <DefaultValue>1.0</DefaultValue>
            </Double>
          </ItemDefinitions>
        </Group>
        <Void Name="WriteStressStrain" Label="Write Stress/Strain Mode Files" Optional="true" IsEnabledByDefault="false" Version="0">
          <BriefDescription>Write the stress/strain .mod files</BriefDescription>
          <Categories>
            <Cat>TEM3P-Eigen</Cat> 
            <Cat>TEM3P-Elastic</Cat> 
            <Cat>TEM3P-ThermoElastic</Cat>
          </Categories>
        </Void>
      </ItemDefinitions>
    </AttDef>

  </Definitions>
</SMTK_AttributeSystem>