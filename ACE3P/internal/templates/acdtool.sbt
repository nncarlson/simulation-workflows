<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Definitions>
    <AttDef Type="AcdtoolMeshconvert" Label="Mesh Convert" Version="0"></AttDef>
    <AttDef Type="AcdtoolMesh" Label="Mesh" Version="0">
      <ItemDefinitions>
        <String Name="MeshTask" Label="Mesh Task" Version="0">
          <ChildrenDefinitions>
            <String Name="MeshOutFilename" Label="Output Mesh Filename" Version="0"></String>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Value Enum="Stats">stats</Value>
            <Value Enum="Check">check</Value>
            <Structure>
              <Value Enum="Fix">fix</Value>
              <Items>
                <Item>MeshOutFilename</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="AcdtoolPostprocess" Label="Postprocess" Version="0">
      <ItemDefinitions>
        <String Name="PostprocessTask" Label="Postprocess Task" Version="0">
          <DiscreteInfo>
            <Value Enum="RF">rf</Value>
            <Value Enum="Generate RF Sample">generate-rf</Value>
            <Value Enum="Eigen To Mode">eigentomode</Value>
            <Value Enum="Volume Monitor to Mode">volmontomode</Value>
            <Value Enum="T3P Longitudinal Wakefield">wake_new</Value>
            <Value Enum="T3P Transverse Wakefield">transwake</Value>
            <Value Enum="T3P Coaxial Signal">coaxsignal</Value>
            <Value Enum="PIC3 Phase Space Statistics">pic3pstats</Value>
            <Value Enum="PIC3 Convert">pic3pconvert</Value>
            <Value Enum="Track3P">track3p</Value>
            <Value Enum="L2 Projections">project</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <!-- Selector attribute - used by selector view-->
    <AttDef Type="AcdtoolSelectorAtt" Label="xyzzy" Version="0">
      <ItemDefinitions>
        <String Name="AcdtoolSelectorItem" Label="Acdtool Task">
          <DiscreteInfo DefaultIndex="0">
            <Value Enum="meshconvert">meshconvert</Value>
            <Value Enum="mesh">mesh</Value>
            <Value Enum="postprocess">postprocess</Value>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Views>
    <View Type="Selector" Name="Acdtool" TopLevel="true" FilterByCategory="false" FilterByAdvanceLevel="false" SelectorName="AcdtoolSelectorItem" SelectorType="AcdtoolSelectorAtt">
      <Views>
        <View Title="meshconvert" Enum="meshconvert"/>
        <View Title="mesh" Enum="mesh"/>
        <View Title="postprocess" Enum="postprocess"/>
      </Views>
    </View>
    <View Type="Instanced" Title="meshconvert">
      <InstancedAttributes>
        <Att Name="AcdtoolMeshConvert1" Type="AcdtoolMeshConvert"/>
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="mesh">
      <InstancedAttributes>
        <Att Name="AcdtoolMesh1" Type="AcdtoolMesh"/>
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="postprocess">
      <InstancedAttributes>
        <Att Name="AcdtoolPostProcess1" Type="AcdtoolPostprocess"/>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeSystem>