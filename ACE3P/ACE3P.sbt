<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">
  <!--**********  Category and Analysis Information ***********-->
  <Categories>
    <Cat>Omega3P</Cat>
    <Cat>S3P</Cat>
    <Cat>T3P</Cat>
    <Cat>Track3P</Cat>
    <Cat>TEM3P-Eigen</Cat>
    <Cat>TEM3P-Harmonic</Cat>
    <Cat>TEM3P-Elastic</Cat>
    <Cat>TEM3P-Linear-Thermal</Cat>
    <Cat>TEM3P-Nonlinear-Thermal</Cat>
    <Cat>TEM3P-ThermoElastic</Cat>
  </Categories>
  <Analyses>
    <Analysis Type="Omega3P Analysis">
      <Cat>Omega3P</Cat>
    </Analysis>
    <Analysis Type="S3P Analysis">
      <Cat>S3P</Cat>
    </Analysis>
    <Analysis Type="T3P Analysis">
      <Cat>T3P</Cat>
    </Analysis>
    <Analysis Type="Track3P Analysis">
      <Cat>Track3P</Cat>
    </Analysis>
    <Analysis Type="TEM3P Eigenmode">
      <Cat>TEM3P-Eigen</Cat>
    </Analysis>
    <Analysis Type="TEM3P Elastic">
      <Cat>TEM3P-Elastic</Cat>
    </Analysis>
    <Analysis Type="TEM3P Harmonic Response">
      <Cat>TEM3P-Harmonic</Cat>
    </Analysis>
    <Analysis Type="TEM3P Thermal Linear">
      <Cat>TEM3P-Linear-Thermal</Cat>
    </Analysis>
    <Analysis Type="TEM3P Thermal Nonlinear">
      <Cat>TEM3P-Nonlinear-Thermal</Cat>
    </Analysis>
    <Analysis Type="TEM3P ThermoElastic">
      <Cat>TEM3P-ThermoElastic</Cat>
    </Analysis>
  </Analyses>

  <!--**********  Include files ***********-->
  <!-- One include file with definitions per tab -->
  <!-- Must load boundaryconditions before analysis, for AttributeRef -->
  <Includes>
    <File>internal/templates/beamloading.sbt</File>
    <File>internal/templates/boundarycondition.sbt</File>
    <File>internal/templates/loading.sbt</File>
    <File>internal/templates/material.sbt</File>
    <File>internal/templates/monitor.sbt</File>
    <File>internal/templates/analysis.sbt</File>
    <File>internal/templates/track3p.sbt</File>
    <File>internal/templates/tem3p.sbt</File>
  </Includes>

  <!--********** Workflow Views ***********-->
  <Views>
    <View Type="Category" Title="SimBuilder" TopLevel="true"
      FilterByAdvanceLevel="false" FilterByCategoryMode="alwaysOn" FilterByCategoryLabel="Module">
      <DefaultColor>1., 1., 0.5, 1.</DefaultColor>
      <InvalidColor>1, 0.5, 0.5, 1</InvalidColor>
      <AdvancedFontEffects />
      <Views>
        <View Title="O3P S3P" Category="Omega3P"/>
        <View Title="O3P S3P" Category="S3P"/>
        <View Title="T3P" Category="T3P"/>
        <View Title="Track3P" Category="Track3P" />
        <View Title="TEM3P Harmonic Response" Category="TEM3P-Harmonic" />
        <View Title="TEM3P Eigenmode" Category="TEM3P-Eigen" />
        <View Title="TEM3P Elastic" Category="TEM3P-Elastic" />
        <View Title="TEM3P Thermal Linear" Category="TEM3P-Linear-Thermal" />
        <View Title="TEM3P Thermal Nonlinear" Category="TEM3P-Nonlinear-Thermal" />
        <View Title="TEM3P ThermoElastic" Category="TEM3P-ThermoElastic" />
      </Views>
    </View>

    <!-- First level views -->
    <View Type="Group" Title="O3P S3P" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="EMBC" />
        <View Title="EM Materials" />
        <View Title="EM Analysis" />
      </Views>
    </View>

    <View Type="Group" Title="T3P" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="EMBC" />
        <View Title="EM Materials" />
        <View Title="EM Analysis" />
        <View Title="Beam Info" />
        <View Title="Loading" />
        <View Title="Monitors" />
      </Views>
    </View>

    <View Type="Group" Title="Track3P" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="Domain" />
        <View Title="Field"/>
        <View Title="Particles"/>
        <!-- <View Title="Track3P BC" /> -->
        <View Title="Track3P Material Model" />
        <View Title="Postprocess"/>
        <!-- <View Title="SEY Curves"/> -->
      </Views>
    </View>

    <View Type="Group" Title="TEM3P Harmonic Response" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="TEM3P Mechanical BC" />
        <View Title="TEM3P Elastic Material" />
        <View Title="TEM3P Elastic Analysis"/>
      </Views>
    </View>

    <View Type="Group" Title="TEM3P Eigenmode" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="TEM3P Mechanical BC" />
        <View Title="TEM3P Elastic Material" />
        <View Title="TEM3P Elastic Analysis"/>
        <View Title="Mesh Output" />
      </Views>
    </View>

    <View Type="Group" Title="TEM3P Elastic" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="TEM3P Mechanical BC" />
        <View Title="TEM3P Elastic Material" />
        <View Title="TEM3P Elastic Analysis"/>
        <View Title="Mesh Output" />
      </Views>
    </View>

    <View Type="Group" Title="TEM3P Thermal Linear" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="TEM3P Thermal BC" />
        <View Title="Thermal Shells" />
        <View Title="TEM3P Thermal Material" />
        <View Title="Heat Sources" />
        <View Title="TEM3P Thermal Analysis"/>
      </Views>
    </View>

    <View Type="Group" Title="TEM3P Thermal Nonlinear" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="TEM3P Thermal BC" />
        <View Title="Thermal Shells" />
        <View Title="TEM3P Thermal Material" />
        <View Title="Heat Sources" />
        <View Title="TEM3P Thermal Analysis"/>
      </Views>
    </View>


    <View Type="Group" Title="TEM3P ThermoElastic" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="TEM3P Elastic" />
        <View Title="TEM3P Thermal Nonlinear" />
      </Views>
    </View>

    <!-- EM Views (O3P, S3P, T3P) -->
    <View Type="Group" Title="EMBC"  Label="Boundary Conditions" Style="Tiled">
      <Views>
        <!-- Hide HFormulation per SLAC request -->
        <!--View Title="HFormulation" / -->
        <View Title="Surface Properties" />
      </Views>
    </View>
    <View Type="Instanced" Title="HFormulation">
      <InstancedAttributes>
        <Att Name="HForumulation" Type="HFormulation" />
      </InstancedAttributes>
    </View>

<!--     <View Type="Attribute" Title="Surface Properties" ModelEntityFilter="f">
      <AttributeTypes>
        <Att Type="SurfaceProperty" />
      </AttributeTypes>
    </View>
 -->
    <View Type="ModelEntity" Title="Surface Properties" ModelEntityFilter="f">
      <AttributeTypes>
        <Att Type="SurfaceProperty" />
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="EM Materials" Label="Materials">
      <AttributeTypes>
        <Att Type="Material" />
      </AttributeTypes>
    </View>

    <View Type="Group" Title="EM Analysis" Label="Analysis" Style="Tiled">
      <Views>
        <!--View Title="Tolerant" /-->
        <View Title="Finite Element" />
        <View Title="Moving Window" />
        <View Title="Frequency Information" />
        <View Title="Post Process" />
        <View Title="High Order Regions" />
      </Views>
    </View>
    <View Type="Instanced" Title="Tolerant">
      <InstancedAttributes>
        <Att Name="Tolerant" Type="Tolerant" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Finite Element">
      <InstancedAttributes>
        <Att Name="Finite Element Info" Type="FEInfo" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Moving Window">
      <InstancedAttributes>
        <Att Name="MovingWindow" Type="MovingWindow" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Frequency Information">
      <InstancedAttributes>
        <Att Name="EigenSolver" Type="FrequencyInfo" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Post Process">
      <InstancedAttributes>
        <Att Name="PostProcess" Type="PostProcess" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="High Order Regions" ModelEntityFilter="r">
      <AttributeTypes>
        <Att Type="RegionHighOrder" />
      </AttributeTypes>
    </View>

    <!-- T3P Specific Views -->
    <View Type="Attribute" Title="Beam Info">
      <AttributeTypes>
        <Att Type="BeamLoading" />
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="Loading">
      <AttributeTypes>
        <Att Type="Loading" />
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="Monitors">
      <AttributeTypes>
        <Att Type="Monitor" />
      </AttributeTypes>
    </View>

    <!-- Track3P-specific views are in templates/track3p.sbt -->
    <!-- TEM3P-specific views are in templates/tem3p.sbt -->
  </Views>
</SMTK_AttributeSystem>
